'use strict'

import * as vscode from 'vscode'
import { window, commands, workspace, Range, Position } from 'vscode'

let currentEditor: vscode.TextEditor

export function activate(context: vscode.ExtensionContext) {
	currentEditor = window.activeTextEditor

	window.onDidChangeActiveTextEditor(editor => (currentEditor = editor))

	const { registerTextEditorCommand: regTEC } = commands
	context.subscriptions.push(
		regTEC('console.log.wrap', () => handle(Wrap.Inline)), regTEC('console.log.wrap.string', () => handle(Wrap.Inline, false, false, FormatAs.String) ),
		regTEC('console.log.wrap.string.up', () => handle(Wrap.Up, false, false, FormatAs.String) ),
		regTEC('console.log.wrap.string.down', () => handle(Wrap.Down, false, false, FormatAs.String) ),
		regTEC('console.log.wrap.prefix', () => handle(Wrap.Inline, true)),
		regTEC('console.log.wrap.input', () => handle(Wrap.Inline, true, true)),
		regTEC('console.log.wrap.up', () => handle(Wrap.Up)),
		regTEC('console.log.wrap.up.prefix', () => handle(Wrap.Up, true)),
		regTEC('console.log.wrap.up.input', () => handle(Wrap.Up, true, true)),
		regTEC('console.log.wrap.down', () => handle(Wrap.Down)),
		regTEC('console.log.wrap.down.prefix', () => handle(Wrap.Down, true)),
		regTEC('console.log.wrap.down.input', () => handle(Wrap.Down, true, true))
	)
}

function handle(
	target: Wrap,
	prefix?: boolean,
	input?: boolean,
	formatAs?: FormatAs
) {
	new Promise((resolve, reject) => {
		let doc = currentEditor.document

		let sel = currentEditor.selection
		let len = sel.end.character - sel.start.character
		let lineNumber = sel.start.line

		const currentLine = doc.lineAt(lineNumber)
		const currentContent = currentLine.text.trim()
		let idx = currentLine.firstNonWhitespaceCharacterIndex

		let ran

		const judgeWordSymbols = ['=', ':', '{', '}']

		if (len) {
			ran = new Range(sel.start, sel.end)
		} else {
			if (currentContent.length) {
				if (judgeWordSymbols.find(el => currentContent.includes(el))) {
					ran = currentEditor.document.getWordRangeAtPosition(sel.anchor)
				} else {
					ran = new Range(
						new Position(lineNumber, idx),
						new Position(lineNumber, currentContent.length)
					)
				}
			}
		}

		if (!ran) {
			reject('NO_WORD')
		}

		let wrapData = {
			item: doc.getText(ran),
			doc,
			ran,
			idx,
			ind: currentLine.text.substring(0, idx),
			line: lineNumber,
			sel,
			txt: undefined,
			lastLine: doc.lineCount - 1 == lineNumber
		}

		prefix = prefix || getSetting('alwaysUsePrefix') ? true : false

		if (prefix) {
			if (getSetting('alwaysInputBoxOnPrefix') || input) {
				window
					.showInputBox({
						placeHolder: 'Prefix string',
						value: '',
						prompt: 'Use text from input box as prefix'
					})
					.then(val => {
						if (val) {
							wrapData.txt = getSetting(
								'format.wrap.prefixFunctionName'
							).concat("('", val.trim(), "',", wrapData.item, ')')
							resolve(wrapData)
						} else reject('INPUT_CANCEL')
					})
			} else {
				wrapData.txt = getSetting('format.wrap.prefixString')
					.replace('$func', getSetting('format.wrap.prefixFunctionName'))
					.replace(/[$]var/g, wrapData.item)
				resolve(wrapData)
			}
		} else {
			if (formatAs) {
				switch (formatAs) {
					case FormatAs.String:
						wrapData.txt = getSetting('format.wrap.logFunctionName').concat(
							"('",
							wrapData.item,
							"')"
						)
						break
				}
			} else {
				wrapData.txt = getSetting('format.wrap.logString')
					.replace('$func', getSetting('format.wrap.logFunctionName'))
					.replace(/[$]var/g, wrapData.item)
			}
			resolve(wrapData)
		}
	})
		.then((wrap: WrapData) => {
			// "Insert and push",
			// "Replace empty"
			const onEmptyAction = getSetting('configuration.emptyLineAction')

			// "Current position",
			// "Beginning of wrap",
			// "End of wrap",
			// "Beginning of Line",
			// "End of line"

			// "Current line",
			// "Target line"
			const setCursorLine = getSetting('configuration.moveToLine')

			function SetCursor(line: number, character?: number) {
				let tpos
				switch (getSetting('configuration.moveToPosition')) {
					case 'Current position':
						tpos = new Position(line, currentEditor.selection.anchor.character)
						break

					case 'End of line':
						tpos = new Position(
							line,
							currentEditor.document.lineAt(line).range.end.character
						)
						break

					case 'Beginning of line':
						tpos = new Position(
							line,
							currentEditor.document.lineAt(line).range.start.character
						)
						break

					case 'Beginning of wrap':
						tpos = new Position(line, character)
						break

					case 'First character':
						tpos = new Position(
							line,
							currentEditor.document.lineAt(
								line
							).firstNonWhitespaceCharacterIndex
						)
						break

					default:
						break
				}
				currentEditor.selection = new vscode.Selection(tpos, tpos)
			}

			function getTargetLine(go: Wrap) {
				let stop = false
				let li = wrap.line
				let l = 0
				while (!stop) {
					if (go == Wrap.Down) {
						li++
					} else {
						li--
					}
					if (li < wrap.doc.lineCount) {
						if (!wrap.doc.lineAt(li).isEmptyOrWhitespace) {
							l = li
							stop = true
						}
					} else {
						if (li == wrap.doc.lineCount) li--
						stop = true
					}
				}
				return li
			}

			switch (target) {
				case Wrap.Inline:
					{
						currentEditor
							.edit(function(e) {
								e.replace(wrap.ran, wrap.txt)
							})
							.then(() => {
								currentEditor.selection = new vscode.Selection(
									new Position(
										wrap.ran.start.line,
										wrap.txt.length + wrap.ran.start.character
									),
									new Position(
										wrap.ran.start.line,
										wrap.txt.length + wrap.ran.start.character
									)
								)
							})
					}
					break

				case Wrap.Up:
					{
						let tLine = wrap.doc.lineAt(wrap.line == 0 ? 0 : wrap.line - 1)
						let tLineEmpty = tLine.text.trim() == '' ? true : false
						var lineCorr = 0

						currentEditor
							.edit(function(e) {
								if (tLineEmpty && onEmptyAction == 'Replace empty') {
									lineCorr = -1
									e.delete(tLine.rangeIncludingLineBreak)
									e.insert(
										new Position(wrap.line, 0),
										wrap.ind.concat(wrap.txt, '\n')
									)
								} else {
									setCursorLine == 'Current line'
										? (lineCorr = 1)
										: (lineCorr = 0)
									if (setCursorLine == 'Target line') {
										e.insert(
											new Position(wrap.line - 1, wrap.idx),
											wrap.ind.concat(wrap.txt)
										)
									} else {
										e.insert(
											new Position(wrap.line, wrap.idx),
											wrap.txt.concat('\n', wrap.ind)
										)
									}
								}
							})
							.then(() => {
								SetCursor(wrap.line + lineCorr, wrap.ind.length)
							})
					}
					break

				case Wrap.Down: {
					let nxtLine: vscode.TextLine
					let nxtLineInd: string
					let nxtNonEmpty: vscode.TextLine

					if (!wrap.lastLine) {
						nxtLine = wrap.doc.lineAt(wrap.line + 1)
						nxtLineInd = nxtLine.text.substring(
							0,
							nxtLine.firstNonWhitespaceCharacterIndex
						)
					} else {
						nxtLineInd = ''
					}

					wrap.ind =
						workspace.getConfiguration('wrap-console-log')['autoFormat'] == true
							? ''
							: wrap.ind
					let pos = new Position(
						wrap.line,
						wrap.doc.lineAt(wrap.line).range.end.character
					)

					currentEditor
						.edit(e => {
							let nxtNonEmpty
							if (nxtLine) {
								nxtNonEmpty = nxtLine.isEmptyOrWhitespace
									? wrap.doc.lineAt(getTargetLine(Wrap.Down))
									: undefined
							}
							if (wrap.lastLine == false && nxtLine.isEmptyOrWhitespace) {
								if (onEmptyAction == 'Insert and push') {
									e.insert(
										new Position(
											wrap.line,
											wrap.doc.lineAt(wrap.line).range.end.character
										),
										'\n'.concat(
											nxtLineInd > wrap.ind ? nxtLineInd : wrap.ind,
											wrap.txt
										)
									)
								} else if (onEmptyAction == 'Replace empty') {
									if (
										nxtLine &&
										nxtNonEmpty.firstNonWhitespaceCharacterIndex > 0
									) {
										e.replace(
											new Position(nxtLine.lineNumber, 0),
											' '
												.repeat(nxtNonEmpty.firstNonWhitespaceCharacterIndex)
												.concat(wrap.txt)
										)
									} else {
										e.replace(
											new Position(nxtLine.lineNumber, 0),
											wrap.ind.concat(wrap.txt)
										)
									}
								}
							} else {
								e.insert(
									new Position(
										wrap.line,
										wrap.doc.lineAt(wrap.line).range.end.character
									),
									'\n'.concat(
										nxtLineInd.length > wrap.ind.length ? nxtLineInd : wrap.ind,
										wrap.txt
									)
								)
							}
						})
						.then(() => {
							if (nxtLine == undefined) {
								nxtLine = wrap.doc.lineAt(wrap.line + 1)
							}
							if (getSetting('autoFormat') == true && !wrap.lastLine) {
								let nextLineEnd = wrap.doc.lineAt(wrap.line + 2).range.end
								currentEditor.selection = new vscode.Selection(
									wrap.sel.start,
									nextLineEnd
								)
								commands
									.executeCommand('currentEditor.action.formatSelection')
									.then(
										() => {
											currentEditor.selection = wrap.sel
										},
										err => {
											window.showErrorMessage(
												"'formatSelection' could not execute propertly"
											)
											console.error(err)
										}
									)
							} else {
								currentEditor.selection = wrap.sel
							}
							SetCursor(
								setCursorLine == 'Current line' ? wrap.line : wrap.line + 1
							)
						})
				}

				default:
					break
			}

			if (getSetting('formatDocument') == true) {
				commands.executeCommand('editor.action.formatDocument')
			}
		})
		.catch(message => {
			console.log('vscode-wrap-console-log CANCEL: ' + message)
		})
}

function getSetting(setting: string) {
	const spl = setting.split('.')

	return spl.length == 1
		? workspace.getConfiguration('wrap-console-log')[setting]
		: spl.splice(1).reduce((a, b) => {
				return a[b]
		  }, workspace.getConfiguration('wrap-console-log')[spl[0]])
}

interface WrapData {
	txt: string
	item: string
	sel: vscode.Selection
	doc: vscode.TextDocument
	ran: Range
	ind: string
	idx: number
	line: number
	lastLine: boolean
}

enum FormatAs {
	String
}

enum Wrap {
	Inline,
	Down,
	Up
}

export function deactivate() {
	return undefined
}
